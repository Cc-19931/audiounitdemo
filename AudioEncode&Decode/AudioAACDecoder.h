//
//  AudioAACEncoder.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AudioAACDecoderDelegate <NSObject>



@end

@interface AudioAACDecoder : NSObject

- (void)decodeAACData:(NSData *)data
           completion:(void(^)(NSError *error,NSData *pcmData))completion;

@end

NS_ASSUME_NONNULL_END
