//
//  AudioUnitCapturer.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AudioUnitCapturerDelegate <NSObject>

- (void)captureAudiobuf:(char *)buf bufLen:(int)bufLen;

@end

@interface AudioUnitCapturer : NSObject
@property(nonatomic,weak) id<AudioUnitCapturerDelegate>delegate;
- (void)starCapture;
- (void)stopCapture;

@end

NS_ASSUME_NONNULL_END
