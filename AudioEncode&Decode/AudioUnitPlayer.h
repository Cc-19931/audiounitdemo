//
//  AudioUnitPlayer.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioUnitPlayer : NSObject

- (void)appendPcmData:(NSData *)pcmData;
- (void)starPlay;
- (void)stopPlay;
@end

NS_ASSUME_NONNULL_END
