//
//  AudioUnitCapturer.m
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import "AudioUnitCapturer.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"

@interface AudioUnitCapturer(){
    AudioComponentInstance _audioUnit;
    AudioStreamBasicDescription _ASBD;
}

@end

@implementation AudioUnitCapturer

#pragma mark - public
- (void)starCapture{
    [self _configCapturer];
    OSStatus result = AudioUnitInitialize(_audioUnit);
    NSLog(@"result %d", result);
    AudioOutputUnitStart(_audioUnit);
}

- (void)stopCapture{
    if (_audioUnit) {
        AudioOutputUnitStop(_audioUnit);
        AudioUnitUninitialize(_audioUnit);
        AudioComponentInstanceDispose(_audioUnit);
        _audioUnit = nil;
    }
}

#pragma mark - private
- (void)_configCapturer{
    
    int kInputBus = 1;
    int kOutputBus = 0;
    AudioComponentDescription io_unit_descr;
    io_unit_descr.componentType = kAudioUnitType_Output;
    io_unit_descr.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    io_unit_descr.componentFlags = 0;
    io_unit_descr.componentFlagsMask = 0;
    io_unit_descr.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    AudioComponent comp = AudioComponentFindNext(NULL, &io_unit_descr);
    OSStatus result = AudioComponentInstanceNew(comp, &_audioUnit);
    
    AudioStreamBasicDescription _ASBD;
    _ASBD.mSampleRate        = AudioSampleRate;
    _ASBD.mFormatID          = kAudioFormatLinearPCM;
    _ASBD.mFormatFlags       = kLinearPCMFormatFlagIsSignedInteger;
    _ASBD.mFramesPerPacket   = 1;
    _ASBD.mChannelsPerFrame  = AudioChannels;
    _ASBD.mBitsPerChannel    = AudioBitDeep;
    _ASBD.mBytesPerFrame     = (_ASBD.mBitsPerChannel / 8) * _ASBD.mChannelsPerFrame;
    _ASBD.mBytesPerPacket    = (_ASBD.mBitsPerChannel / 8) * _ASBD.mChannelsPerFrame * _ASBD.mFramesPerPacket;
    
    // 设置 element1 的 outputScope ASBD
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &_ASBD,
                                  sizeof(_ASBD));
    
    // 设置 element0 的 inputScope ASBD
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &_ASBD,
                                  sizeof(_ASBD));
    
    // 设置 element1 的 inputScope  录音开关
    UInt32 flag = 1;
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  kInputBus,
                                  &flag,
                                  sizeof(flag));

    // 设置element0 的 outputScope 的扬声器开关
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &_ASBD,
                                  sizeof(_ASBD));
    
    AURenderCallbackStruct rc;
    memset(&rc, 0, sizeof(rc));
    rc.inputProc = captureCallback;
    rc.inputProcRefCon = (__bridge void * _Nullable)(self);
    
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  kInputBus,
                                  &rc,
                                  sizeof(rc));
    
    AURenderCallbackStruct pc;
    memset(&pc, 0, sizeof(pc));
    pc.inputProc = PlayCallbackFunction;
    pc.inputProcRefCon = (__bridge void * _Nullable)(self);
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global, kOutputBus,
                                  &pc, sizeof(pc));
     
    
    flag = 0;
    result = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &flag,
                                  sizeof(flag));
     
    // 回声消除
    UInt32 echoCancellation = 0;
    result = AudioUnitSetProperty(_audioUnit,
                                    kAUVoiceIOProperty_BypassVoiceProcessing,
                                    kAudioUnitScope_Global,
                                    0,
                                    &echoCancellation,
                                    sizeof(echoCancellation));
    // 增益
    UInt32 agcFlag = 0;
    result = AudioUnitSetProperty(_audioUnit,
                                  kAUVoiceIOProperty_VoiceProcessingEnableAGC,
                                  kAudioUnitScope_Global,
                                  kOutputBus,
                                  &agcFlag,
                                  sizeof(agcFlag));
     
}

static OSStatus captureCallback(void *inRefCon,
                               AudioUnitRenderActionFlags *ioActionFlags,
                               const AudioTimeStamp *inTimeStamp,
                               UInt32 inBusNumber,
                               UInt32 inNumberFrames,//帧数
                               AudioBufferList *ioData){
    AudioUnitCapturer *capture = (__bridge AudioUnitCapturer *)inRefCon;
    
    AudioBuffer buffer;
    buffer.mNumberChannels = AudioChannels;
    buffer.mDataByteSize = inNumberFrames * (AudioBitDeep / 8) * buffer.mNumberChannels;
    buffer.mData = malloc(inNumberFrames * (AudioBitDeep / 8) * buffer.mNumberChannels);
    
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    AudioUnitRender(capture->_audioUnit,
                    ioActionFlags,
                    inTimeStamp,
                    inBusNumber,
                    inNumberFrames,
                    &bufferList);
    
    NSLog(@"audioUnit capture pcmData success Size:%d",bufferList.mBuffers[0].mDataByteSize);
    if (capture && [capture.delegate respondsToSelector:@selector(captureAudiobuf:bufLen:)]) {
        [capture.delegate captureAudiobuf:bufferList.mBuffers[0].mData bufLen:bufferList.mBuffers[0].mDataByteSize];
    }
    free(bufferList.mBuffers[0].mData);
    return noErr;
}

static OSStatus PlayCallbackFunction (void *                            inRefCon,
                                      AudioUnitRenderActionFlags *    ioActionFlags,
                                      const AudioTimeStamp *            inTimeStamp,
                                      UInt32                            inBusNumber,
                                      UInt32                            inNumberFrames,
                                      AudioBufferList * __nullable    ioData){
    return 0;
}

@end
