//
//  AudioAACEncoder.m
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import "AudioAACDecoder.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"

@interface AudioAACDecoder()

@property (nonatomic) AudioBufferList inputBufferList;
@property (nonatomic) AudioBufferList *resBufferList;
@property (nonatomic) AudioStreamPacketDescription *currentAudioStreamPacketDescription;
@property (nonatomic) AudioStreamBasicDescription in_ASBDes;
@property (nonatomic) AudioStreamBasicDescription out_ASBDes;
@property (nonatomic, unsafe_unretained) AudioConverterRef audioConverter;
@property (nonatomic) AudioStreamPacketDescription *audioPacketFormat;

@end

@implementation AudioAACDecoder

- (instancetype)init{
    if (self = [super init]) {
        [self _config];
    }
    return self;
}

- (void)_config{
    self.currentAudioStreamPacketDescription = malloc(sizeof(AudioStreamPacketDescription));
    
    AudioStreamBasicDescription AACAudioDes ={0};
    AACAudioDes.mSampleRate = AudioSampleRate;
    AACAudioDes.mFormatID = kAudioFormatMPEG4AAC;
    AACAudioDes.mFormatFlags = kMPEG4Object_AAC_LC;
    AACAudioDes.mBytesPerPacket = 0;
    AACAudioDes.mFramesPerPacket = 1024;
    AACAudioDes.mBytesPerFrame = 0;
    AACAudioDes.mChannelsPerFrame = AudioChannels;
    AACAudioDes.mBitsPerChannel = 0;
    AACAudioDes.mReserved = 0;
    self.in_ASBDes = AACAudioDes;
    
    AudioStreamBasicDescription PCMAudioDes ={0};
    PCMAudioDes.mSampleRate = AudioSampleRate;
    PCMAudioDes.mFormatID = kAudioFormatLinearPCM;
    PCMAudioDes.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    PCMAudioDes.mFramesPerPacket = 1;
    PCMAudioDes.mBitsPerChannel = AudioBitDeep;
    PCMAudioDes.mChannelsPerFrame = AudioChannels;
    PCMAudioDes.mBytesPerFrame = (PCMAudioDes.mBitsPerChannel / 8) * PCMAudioDes.mChannelsPerFrame;
    PCMAudioDes.mBytesPerPacket = (PCMAudioDes.mBitsPerChannel / 8) * PCMAudioDes.mChannelsPerFrame * PCMAudioDes.mFramesPerPacket;
    PCMAudioDes.mReserved = 0;
    self.out_ASBDes = PCMAudioDes;
    
    self.audioPacketFormat = malloc(sizeof(AudioStreamPacketDescription));
    
    // create an audio converter
    AudioStreamBasicDescription inputAduioDes = self.in_ASBDes;
    AudioStreamBasicDescription outputAudioDes = self.out_ASBDes;
    AudioConverterRef audioConverter;
    OSStatus acCreationResult = AudioConverterNew(&inputAduioDes,
                                                  &outputAudioDes,
                                                  &audioConverter);
    if (acCreationResult) {
        NSLog(@"aacToPcm converter creat error");
    }
    _audioConverter = audioConverter;
}

- (void)decodeAACData:(NSData *)data
           completion:(void (^)(NSError * _Nonnull, NSData * _Nonnull))completion{
    
    
    @autoreleasepool {
        
        //输入
        char *p = malloc(data.length);
        memcpy(p, data.bytes + 7, data.length - 7);
        AudioBufferList inputBufferList;
        inputBufferList.mNumberBuffers = 1;
        inputBufferList.mBuffers[0].mNumberChannels = 1;
        inputBufferList.mBuffers[0].mData = p;
        inputBufferList.mBuffers[0].mDataByteSize = (UInt32)data.length - 7;
        self.inputBufferList = inputBufferList;
        
        //输出
        AudioBufferList bufferList;
        bufferList.mNumberBuffers = 1;
        bufferList.mBuffers[0].mNumberChannels = 1;
        bufferList.mBuffers[0].mData = malloc(1024 * (AudioBitDeep / 8) * bufferList.mBuffers[0].mNumberChannels);
        bufferList.mBuffers[0].mDataByteSize = (UInt32)1024 * (AudioBitDeep / 8) * bufferList.mBuffers[0].mNumberChannels;
        
        UInt32 ioOutputDataPacketSize = (UInt32)1024;
        
        NSError *error = nil;
        OSStatus status;
        AudioStreamPacketDescription *aspd = NULL;
        status = AudioConverterFillComplexBuffer(self->_audioConverter,
                                                 inputDataProc,
                                                 (__bridge void * _Nullable)(self),
                                                 &ioOutputDataPacketSize,
                                                 &bufferList,
                                                 aspd);
        if (status == noErr) {
            NSLog(@"audio aacToPcm conver success, size:%d",bufferList.mBuffers[0].mDataByteSize);
        }else {
            error = [NSError errorWithDomain:NSOSStatusErrorDomain code:status userInfo:nil];
        }
        
        if (error) {
            NSLog(@"audio aacToPcm convert error: %@",error);
        }
        
        if (completion) {
            completion(error,[NSData dataWithBytes:bufferList.mBuffers[0].mData length:bufferList.mBuffers[0].mDataByteSize]);
        }
    }
}

static OSStatus inputDataProc(AudioConverterRef inAudioConverter,
                              UInt32 *ioNumberDataPackets,
                              AudioBufferList *ioData,
                              AudioStreamPacketDescription **outDataPacketDescription,
                              void *inUserData) {
    
    AudioAACDecoder *thisSelf = (__bridge AudioAACDecoder *)(inUserData);

#warning if decode aac, must set outDataPacketDescription !!!
    UInt32 l = thisSelf.inputBufferList.mBuffers[0].mDataByteSize;
    if (outDataPacketDescription != NULL) {
        *outDataPacketDescription = thisSelf.audioPacketFormat;
        (*outDataPacketDescription)[0].mStartOffset             = 0;
        (*outDataPacketDescription)[0].mDataByteSize            = l;
        (*outDataPacketDescription)[0].mVariableFramesInPacket  = 0;
    }
    
    ioData->mNumberBuffers              = 1;
    ioData->mBuffers[0].mNumberChannels = AudioChannels;
    ioData->mBuffers[0].mData           = thisSelf.inputBufferList.mBuffers[0].mData;
    ioData->mBuffers[0].mDataByteSize   = thisSelf.inputBufferList.mBuffers[0].mDataByteSize;
    
    return noErr;
}

@end
