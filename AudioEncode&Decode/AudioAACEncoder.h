//
//  AudioAACDecoder.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AudioAACEncoderDelegate <NSObject>

- (void)encodeProcessAACData:(NSData *)data;

@end

@interface AudioAACEncoder : NSObject

@property(nonatomic,weak)id<AudioAACEncoderDelegate>delegate;

- (void)encodePcmData:(char *)pcmData
              dataLen:(int)dataLen
           completion:(void(^)(NSError * __nullable,NSData * __nullable))completion;

NS_ASSUME_NONNULL_END
@end
