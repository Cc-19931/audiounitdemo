//
//  AudioAACDecoder.m
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import "AudioAACEncoder.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"

@interface AudioAACEncoder(){
    AudioConverterRef _convertRef;
    char *_leftBuffer; // pcm缓冲
    NSInteger _leftBufferLen; //pcm长度
    char *_aacBuffer; // aac缓冲
    NSInteger _bufferLen; // 每次送编码的长度
}

@end

@implementation AudioAACEncoder

- (instancetype)init{
    if (self = [super init]) {
        [self _configAudioConvert];
    }
    return self;
}

#pragma mark - private
- (void)_configAudioConvert{
    //设置输入pcm的asbd
    AudioStreamBasicDescription inputASBD;
    inputASBD.mSampleRate           = AudioSampleRate;
    inputASBD.mFormatID             = kAudioFormatLinearPCM;
    inputASBD.mFormatFlags          = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked; // 下面这个是保存音频数据的方式的说明，如可以根据大端字节序或小端字节序，浮点数或整数以及不同体位去保存数据;
    inputASBD.mChannelsPerFrame     = AudioChannels;
    inputASBD.mFramesPerPacket      = 1;
    inputASBD.mBitsPerChannel       = AudioBitDeep;
    inputASBD.mBytesPerFrame        = (inputASBD.mBitsPerChannel / 8) * inputASBD.mChannelsPerFrame;
    inputASBD.mBytesPerPacket       = (inputASBD.mBitsPerChannel / 8) * inputASBD.mChannelsPerFrame * inputASBD.mFramesPerPacket;
    
    //设置输出的adbd
    AudioStreamBasicDescription outPutASBD;
    outPutASBD.mSampleRate          = AudioSampleRate;
    outPutASBD.mFormatID            = kAudioFormatMPEG4AAC;
    outPutASBD.mFormatFlags         = kMPEG4Object_AAC_LC;
    outPutASBD.mChannelsPerFrame    = AudioChannels;
    outPutASBD.mFramesPerPacket     = 1024;
    //语音每采样点占用位数 压缩格式设置为0
    outPutASBD.mBitsPerChannel      = 0;
    //每帧的bytes数，每帧的大小。每一帧的起始点到下一帧的起始点。如果是压缩格式，设置为0 。
    outPutASBD.mBytesPerFrame       = 0;
    //每一个packet的音频数据大小。如果的动态大小设置为0。动态大小的格式需要用AudioStreamPacketDescription来确定每个packet的大小。
    outPutASBD.mBytesPerPacket      = 0;
    //8字节对齐，填0.
    outPutASBD.mReserved            = 0;
    
    //设置编码参数
    AudioClassDescription ACD;
    OSStatus status;
    UInt32 encoderSpecifier = kAudioFormatMPEG4AAC;
    UInt32 size;
    //这里拿到总编码器的大小 size
    status = AudioFormatGetPropertyInfo(kAudioFormatProperty_Encoders,
                                        sizeof(encoderSpecifier),
                                        &encoderSpecifier,
                                        &size);
    if (status) {
        NSLog(@"拿到audio编码器的总大小失败");
    }
    
    uint count = size/sizeof(AudioClassDescription);
    AudioClassDescription ACDesc[count];
    //拿编码器的数组
    status = AudioFormatGetProperty(kAudioFormatProperty_Encoders,
                                    sizeof(encoderSpecifier),
                                    &encoderSpecifier,
                                    &size,
                                    ACDesc);
    if (status) {
        NSLog(@"拿到audio编码器数组失败");
    }
    
    //找到匹配硬编码的编码器
    for (int i = 0; i < count; i++) {
        if (ACDesc[i].mSubType == kAudioFormatMPEG4AAC &&
            ACDesc[i].mManufacturer == kAppleHardwareAudioCodecManufacturer) {
            memcpy(&ACD, &(ACDesc[i]), sizeof(AudioClassDescription));
            break;
        }
    }

    //创建编码器
    status = AudioConverterNewSpecific(&inputASBD,
                                       &outPutASBD,
                                       1,
                                       &ACD,
                                       &_convertRef);
    if (status) {
        NSLog(@"audio编码器初始化失败");
    }
    
    UInt32 ulBitRate = 64000;
    if (inputASBD.mSampleRate == 16000) {
        ulBitRate = 16000;
    }
    UInt32 ulSize = sizeof(ulBitRate);
    status = AudioConverterSetProperty(_convertRef,
                                       kAudioConverterEncodeBitRate,
                                       ulSize,
                                       &ulBitRate);
    
    if (status != 0) {
        NSLog(@"AudioConverterSetProperty failure: %d", (int)status);
    }
    
    // 5、设置每次送给编码器的数据长度。
    // 这里设置每次送给编码器的数据长度为：1024 * 2(16 bit 采样深度) * 声道数量
    // 因为我们每次调用 AudioConverterFillComplexBuffer 编码时，是送进去一个包（packet），而对于 AAC 来讲，mFramesPerPacket 需要是 1024，即 1 个 packet 有 1024 帧，而每个音频帧的大小是：2(16 bit 采样深度) * 声道数量。
    _bufferLen = 1024 * (AudioBitDeep/8.0) * AudioChannels;
    
    // 6、初始化待编码缓冲区和编码缓冲区。
    if (!_leftBuffer) {
        // 待编码缓冲区长度达到 _bufferLength，就会送一波给编码器，所以大小 _bufferLength 够用了。
        _leftBuffer = malloc(_bufferLen);
    }
    if (!_aacBuffer) {
        // AAC 编码缓冲区只要装得下 _bufferLength 长度的 PCM 数据编码后的数据就好了，编码是压缩，所以大小 _bufferLength 也够用了。
        _aacBuffer = malloc(_bufferLen);
    }
}


- (NSData *)_configADTSHeaderWithPayloadLen:(int)payloadLen{
    
    //封装 ADTS header
    int adtsLength = 7;
    char *packet = malloc(sizeof(char) * adtsLength);
    // Variables Recycled by addADTStoPacket
    int profile = 2;  //AAC LC
    //39=MediaCodecInfo.CodecProfileLevel.AACObjectELD;
    int freqIdx = 4;  //44.1KHz
    int chanCfg = 1;  //MPEG-4 Audio Channel Configuration. 1 Channel front-center
    NSUInteger fullLength = adtsLength + payloadLen;
    // fill in ADTS data
    packet[0] = (char)0xFF; // 11111111     = syncword
    packet[1] = (char)0xF9; // 1111 1 00 1  = syncword MPEG-2 Layer CRC
    packet[2] = (char)(((profile-1)<<6) + (freqIdx<<2) +(chanCfg>>2));
    packet[3] = (char)(((chanCfg&3)<<6) + (fullLength>>11));
    packet[4] = (char)((fullLength&0x7FF) >> 3);
    packet[5] = (char)(((fullLength&7)<<5) + 0x1F);
    packet[6] = (char)0xFC;
    NSData *data = [NSData dataWithBytes:packet length:adtsLength];
    free(packet);
    return data;
}

#pragma mark - public
- (void)encodePcmData:(char *)pcmData
              dataLen:(int)dataLen
           completion:(nonnull void (^)(NSError * _Nullable, NSData * _Nullable))completion{
    
    // 当待编码缓冲区遗留数据加上新来的数据长度(_leftLength + dataLen)大于每次给编码器的数据长度(_bufferLen)时，则进行循环编码，每次送给编码器长度为 _bufferLen 的数据量。
    if (_leftBufferLen + dataLen >= _bufferLen) {
        NSInteger totalSize = _leftBufferLen + dataLen;
        // 计算给编码器送数据的次数。
        NSInteger encodeCount = totalSize / _bufferLen;
        char *totalBuf = malloc(totalSize);
        memset(totalBuf, 0, totalSize);
        memcpy(totalBuf, _leftBuffer, _leftBufferLen); //拷贝上次遗留数据
        memcpy(totalBuf + _leftBufferLen, pcmData, dataLen); //拷贝此次传进来的pcm
        
        for (int i = 0; i < encodeCount; i++) {
            
            char *p = malloc(_bufferLen);
            memcpy(p, totalBuf + i * _bufferLen, _bufferLen);
            //初始化一个缓冲队列
            AudioBufferList bufferList;
            bufferList.mNumberBuffers = 1;
            bufferList.mBuffers[0].mNumberChannels = 1;
            bufferList.mBuffers[0].mData = malloc(_bufferLen);
            bufferList.mBuffers[0].mDataByteSize = (UInt32)_bufferLen;
            
            AudioBufferList inputBufferList;
            inputBufferList.mNumberBuffers = 1;
            inputBufferList.mBuffers[0].mNumberChannels = 1;
            inputBufferList.mBuffers[0].mData = p;
            inputBufferList.mBuffers[0].mDataByteSize = (UInt32)_bufferLen;
            
            UInt32 outPutDataPackerSize = 1;
            AudioStreamPacketDescription *aspd = NULL;
            OSStatus status = AudioConverterFillComplexBuffer(_convertRef,
                                                              converProcess,
                                                              &inputBufferList,
                                                              &outPutDataPackerSize,
                                                              &bufferList,
                                                              aspd);
            
            NSError *error = nil;
            if (status) {
                NSLog(@"audio encode AAC failure, status:%d",status);
                error = [NSError errorWithDomain:NSOSStatusErrorDomain code:status userInfo:nil];
                if (completion) {
                    completion(error,nil);
                }
            }else{
                NSData *rawAAC = [NSData dataWithBytes:bufferList.mBuffers[0].mData length:bufferList.mBuffers[0].mDataByteSize];
                NSData *headerData = [self _configADTSHeaderWithPayloadLen:(int)rawAAC.length];
                NSMutableData *adtsData = [NSMutableData dataWithData:headerData];
                [adtsData appendData:rawAAC];
                NSLog(@"audio encode AAC success size :%zd",adtsData.length);
                if (self.delegate &&
                    [self.delegate respondsToSelector:@selector(encodeProcessAACData:)]) {
                    [self.delegate encodeProcessAACData:adtsData];
                }
                if (completion) {
                    completion(nil,adtsData);
                }
            }
            
            free(bufferList.mBuffers[0].mData);
            free(p);
        }
        
        _leftBufferLen = totalSize % _bufferLen;
        memset(_leftBuffer, 0, _bufferLen);
        memcpy(_leftBuffer, totalBuf + (totalSize - _leftBufferLen), _leftBufferLen);
        free(totalBuf);
        
    }else{
        memcpy(_leftBuffer + _leftBufferLen, pcmData, dataLen);
        _leftBufferLen += dataLen;
    }
}

OSStatus converProcess(AudioConverterRef inConverter,
                        UInt32 *ioNumberDataPackets,
                        AudioBufferList *ioData,
                        AudioStreamPacketDescription **outDataPacketDescription,
                        void *inUserData){
    
    //填充pcm数据
    //inUserData 是传进来的input pcm data
    AudioBufferList buffList = *(AudioBufferList *)inUserData;
    
    ioData->mNumberBuffers              = 1;
    ioData->mBuffers[0].mData           = buffList.mBuffers[0].mData;
    ioData->mBuffers[0].mDataByteSize   = buffList.mBuffers[0].mDataByteSize;
    ioData->mBuffers[0].mNumberChannels = AudioChannels;
    
    return noErr;
}

@end
