//
//  Constants.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#ifndef Constants_h
#define Constants_h

#define AudioSampleRate 16000
#define AudioChannels 1
#define AudioBitDeep 16

#endif /* Constants_h */
