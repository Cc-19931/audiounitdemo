//
//  ViewController.m
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AudioUnitCapturer.h"
#import "AudioAACEncoder.h"
#import "AudioAACDecoder.h"
#import "AudioUnitPlayer.h"

@interface ViewController ()<AudioUnitCapturerDelegate,AudioAACEncoderDelegate>{
    NSMutableData *_streamData;
}

@property(nonatomic,strong)AudioUnitCapturer *audioCapture;
@property(nonatomic,strong)AudioAACEncoder *aacEncoder;
@property(nonatomic,strong)AudioAACDecoder *aacDecoder;
@property(nonatomic,strong)AudioUnitPlayer *audioPlayer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *speakBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    speakBtn.frame = CGRectMake(self.view.frame.size.width/2 - 50, 100, 100, 30);
    [speakBtn setTitle:@"star" forState:UIControlStateNormal];
    [speakBtn setTitle:@"stop" forState:UIControlStateSelected];
    [speakBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [speakBtn setBackgroundColor:UIColor.orangeColor];
    [speakBtn addTarget:self action:@selector(speakBtnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:speakBtn];
}

- (void)speakBtnClickAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    
    if (btn.selected) {
        [self _setCaptureAudioSession];
        [self.audioCapture starCapture];
    }else{
        [self _setPlayBackAudioSession];
        [self.audioCapture stopCapture];
    }
}

- (void)_setCaptureAudioSession{
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord
             withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetoothA2DP
                   error:nil];
    // 采样率
    [session setPreferredSampleRate:16000 error:nil];
    NSTimeInterval bufferDuration = 0.064;
    [session setPreferredIOBufferDuration:bufferDuration error:nil];
    [session setMode:AVAudioSessionModeVideoChat error:nil];
    [session setActive:YES error:nil];
}

- (void)_setPlayBackAudioSession{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback
             withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetoothA2DP
                   error:nil];
    [session setActive:YES error:nil];
}

#pragma mark - AudioUnitCapturerDelegate
// audioUnit 采集 pcm
- (void)captureAudiobuf:(char *)buf bufLen:(int)bufLen{
    // 拿 pcm 编码 aac
    [self.aacEncoder encodePcmData:buf
                           dataLen:bufLen
                        completion:^(NSError * _Nonnull error, NSData * _Nonnull aacData) {
        if (!error) {
            // 拿 aac 解码 pcm
            [self.aacDecoder decodeAACData:aacData
                                completion:^(NSError * _Nonnull error, NSData * _Nonnull pcmData) {

                if (!error) {
                    // audioUnit 播放 pcm
                    [self.audioPlayer appendPcmData:pcmData];
                    [self.audioPlayer starPlay];
                }
            }];
        }
    }];
}

#pragma mark - AudioAACEncoderDelegate
- (void)encodeProcessAACData:(NSData *)AACData{

}

#pragma mark - lazyLoad
- (AudioUnitCapturer *)audioCapture{
    if (!_audioCapture) {
        _audioCapture = [[AudioUnitCapturer alloc] init];
        _audioCapture.delegate = self;
    }
    return _audioCapture;
}

- (AudioAACEncoder *)aacEncoder{
    if (!_aacEncoder) {
        _aacEncoder = [[AudioAACEncoder alloc] init];
        _aacEncoder.delegate = self;
    }
    return _aacEncoder;
}

- (AudioAACDecoder *)aacDecoder{
    if (!_aacDecoder) {
        _aacDecoder = [[AudioAACDecoder alloc] init];
    }
    return _aacDecoder;
}

-(AudioUnitPlayer *)audioPlayer{
    if (!_audioPlayer) {
        _audioPlayer = [[AudioUnitPlayer alloc] init];
    }
    return _audioPlayer;
}

@end
