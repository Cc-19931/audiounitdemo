//
//  AudioUnitPlayer.m
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import "AudioUnitPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"

@interface AudioUnitPlayer (){
    AudioComponentInstance _audioUnit;
    dispatch_queue_t _audioUnitPlayQueue;
    bool _isPlaying;
    NSMutableData *_pcmData;
}

@end

@implementation AudioUnitPlayer


- (void)starPlay{
    if (_isPlaying) return;
    _isPlaying = YES;
    
    if (!_audioUnitPlayQueue) {
        _audioUnitPlayQueue = dispatch_queue_create("audioUnitPlayer", DISPATCH_QUEUE_SERIAL);
    }
    
    AudioComponentDescription audioDesc;
    audioDesc.componentType = kAudioUnitType_Output;
    audioDesc.componentSubType = kAudioUnitSubType_RemoteIO;
    audioDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
    audioDesc.componentFlags = 0;
    audioDesc.componentFlagsMask = 0;
    //audioComponent 初始化audioUnit
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &audioDesc);
    AudioComponentInstanceNew(inputComponent, &_audioUnit);
    
    OSStatus status = noErr;
    UInt32 flag = 1;
    //开启element0 的 outPutScope（连接手机的扬声器）：默认时开启的，不设置也没有问题
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  0,
                                  &flag,
                                  sizeof((flag)));
    if (status) {
        NSLog(@"audioPlay 设置 element0 outputScope enableIO 出错 status:%d", status);
    }
    
    // 设置element0 的 inputScope的 ASBD
    AudioStreamBasicDescription inputASBD;
    memset(&inputASBD, 0, sizeof(inputASBD));
    inputASBD.mSampleRate       = AudioSampleRate; // 采样率
    inputASBD.mFormatID         = kAudioFormatLinearPCM; // PCM格式
    inputASBD.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger; // 整形
    inputASBD.mFramesPerPacket  = 1; // 每帧只有1个packet
    inputASBD.mChannelsPerFrame = AudioChannels; // 声道数
    inputASBD.mBitsPerChannel   = AudioBitDeep; // 位深
    inputASBD.mBytesPerFrame    = inputASBD.mChannelsPerFrame * (inputASBD.mBitsPerChannel / 8);
    inputASBD.mBytesPerPacket   = inputASBD.mChannelsPerFrame * (inputASBD.mBitsPerChannel / 8) * inputASBD.mFramesPerPacket;
    
    //用配置好的ASBD设置element0的property
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  0,
                                  &inputASBD,
                                  sizeof(inputASBD));
    if (status) {
        NSLog(@"audioPlay 设置 element0 inputScope ASBD 出错 status:%d", status);
    }
    
    //设置回调函数
    AURenderCallbackStruct playCallBack;
    playCallBack.inputProc = PlayCallback;
    playCallBack.inputProcRefCon = (__bridge void *)self;
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Input,
                                  0,
                                  &playCallBack,
                                  sizeof(playCallBack));
    
    if (status) {
        NSLog(@"audioPlay 设置 callBack 出错 status:%d",status);
    }
    
    //最后初始化audioUnit
    OSStatus result = AudioUnitInitialize(_audioUnit);
    NSLog(@"result %d", result);
    
    AudioOutputUnitStart(_audioUnit);
}

#pragma mark - element0 的input的回调函数
static OSStatus PlayCallback(void *inRefCon,
                             AudioUnitRenderActionFlags *ioActionFlags,
                             const AudioTimeStamp *inTimeStamp,
                             UInt32 inBusNumber,
                             UInt32 inNumberFrames,
                             AudioBufferList *ioData){
    
    AudioUnitPlayer *player = (__bridge AudioUnitPlayer *)inRefCon;
    dispatch_async(player->_audioUnitPlayQueue, ^{
        NSLog(@"audioUnit play pcm data size: %d", ioData->mBuffers[0].mDataByteSize);
        if (player->_pcmData.length < ioData->mBuffers[0].mDataByteSize) {
            ioData->mBuffers[0].mDataByteSize = (UInt32)player->_pcmData.length;
        }
        memcpy(ioData->mBuffers[0].mData, player->_pcmData.bytes, ioData->mBuffers[0].mDataByteSize);
        [player->_pcmData replaceBytesInRange:NSMakeRange(0, ioData->mBuffers[0].mDataByteSize) withBytes:NULL length:0];
    });
    
    return noErr;
}

- (void)stopPlay{
    AudioOutputUnitStop(_audioUnit);
    if (_audioUnitPlayQueue) {
        _audioUnitPlayQueue = 0;
    }
    _isPlaying = NO;
}

- (void)appendPcmData:(NSData *)pcmData{
    
    if (!_audioUnitPlayQueue) {
        _audioUnitPlayQueue = dispatch_queue_create("audioUnitPlayer", DISPATCH_QUEUE_SERIAL);
    }
    
    if (!_pcmData) {
        _pcmData = [NSMutableData data];
    }
    
    dispatch_async(_audioUnitPlayQueue, ^{
        [self->_pcmData appendData:pcmData];
    });
}

- (void)dealloc{
    AudioOutputUnitStop(_audioUnit);
    AudioUnitUninitialize(_audioUnit);
    AudioComponentInstanceDispose(_audioUnit);
}
@end
