//
//  SceneDelegate.h
//  AudioEncode&Decode
//
//  Created by xupanpan on 2022/7/13.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

